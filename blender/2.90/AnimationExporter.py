# Proxy Studios Gladius Animation Exporter (Blender 2.90)
# Copyright (C) 2019-2020 Proxy Studios
# All rights reserved

import bpy
import csv
import os.path
import pathlib
import re
from .MeshExporter import getRotatedMatrix

# Export frames
def saveAnimation(self, mesh, outputFile, frameStart, frameEnd):
    arm = mesh.parent
    if arm is None:
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! Mesh does not have an associated armature!')
        return {'FINISHED'}
    if not arm.animation_data or not arm.animation_data.action:
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! No animation data!')
        return {'FINISHED'}
    batchProcess = True
    if frameStart is None or frameEnd is None:
        batchProcess = False
        frameStart = self.ps_animation_frame_start
        frameEnd = self.ps_animation_frame_end
    frames = (frameEnd - frameStart) + 1
    frameRate = bpy.context.scene.render.fps
    outputFile.write('<?xml version=\"1.0\" encoding=\"utf-8\"?>\n')
    outputFile.write('<animation version=\"0.9\" frames=\"%d\" frameRate=\"%d\">\n' % (frames, frameRate))
    reorderedIndex = -1
    mesh_group_names = [g.name for g in mesh.vertex_groups]
    bones = []
    for b in arm.data.bones:
        bones.append(b.name)
    bones.sort(key = str.lower)
    for index,b in enumerate(bones):
        if b not in mesh_group_names:
            continue
        reorderedIndex += 1
        outputFile.write('\t<bone id=\"%d\" name=\"%s\">\n' % (reorderedIndex, bones[index]))
        for poseBone in arm.pose.bones:
            if poseBone.name == bones[index]:
                mb = arm.matrix_world @ poseBone.bone.matrix_local
                pb = mb.to_translation()
                rmb = getRotatedMatrix(mb)
                sb = rmb.to_scale()
                for f in range(frameStart,frameEnd + 1):
                    bpy.context.scene.frame_set(f)
                    m = arm.matrix_world @ poseBone.matrix
                    p = m.to_translation() - pb
                    rm = getRotatedMatrix(m)
                    r = (rm.to_3x3() @ rmb.to_3x3().inverted()).to_quaternion()
                    s = rm.to_scale()
                    sx = s.x / sb.x
                    sy = s.y / sb.y
                    sz = s.z / sb.z
                    outputFile.write('\t\t<frame id=\"%d\">\n' % f)
                    outputFile.write('\t\t\t<position x=\"%f\" y=\"%f\" z=\"%f\"/>\n' % (p.x, p.y, p.z))
                    outputFile.write('\t\t\t<rotation x=\"%f\" y=\"%f\" z=\"%f\" w=\"%f\"/>\n' % (r.x, r.y, r.z, r.w))
                    outputFile.write('\t\t\t<scale x=\"%f\" y=\"%f\" z=\"%f\"/>\n' % (sx, sy, sz))
                    outputFile.write('\t\t</frame>\n')
                continue
        outputFile.write('\t</bone>\n')
    outputFile.write('</animation>\n')
    if not batchProcess:
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'SUCCESS! Exported animation frames ' + str(frameStart) + '-' + str(frameEnd) + '.')
    return {'FINISHED'}

# Export animations by batch
def saveBatchAnimation(self):
    if os.path.isdir(self.properties.filepath):
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! Must supply an input file for batch exporting!')
        return {'CANCELLED'}
    inputFile = open(self.properties.filepath, 'r')
    pattern = re.compile(r'^([0-9]+)-([0-9]+)$')
    reader = csv.reader(inputFile)
    headings = next(reader)
    for row in reader:
        if bpy.data.objects.get(row[0]) is not None:
            mesh = bpy.data.objects[row[0]]
            for i in range(1, len(headings)):
                frameRange = re.search(pattern, row[i])
                if frameRange:
                    frameStart = int(frameRange.group(1))
                    frameEnd = int(frameRange.group(2))
                    fileName = row[0] + headings[i] + '.xml'
                    fileDirectory = pathlib.PurePath(os.path.dirname(self.properties.filepath))
                    outputFile = open(os.path.join(fileDirectory, fileName), 'w+')
                    saveAnimation(self, mesh, outputFile, frameStart, frameEnd)
                    outputFile.close()
    inputFile.close()
    bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'SUCCESS! Exported batch animations.')
    return {'FINISHED'}

# Export animations
def save(self):
    if getattr(self, 'ps_animation_batch_export'):
        saveBatchAnimation(self)
        return {'FINISHED'}
    if len(bpy.context.selected_objects) == 0:
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! No mesh selected!')
        return {'FINISHED'}
    if len(bpy.context.selected_objects) > 1:
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! You must select only a single mesh!')
        return {'FINISHED'}
    obj = bpy.context.selected_objects[0]
    if obj.type not in 'MESH':
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! No mesh selected!')
        return {'FINISHED'}
    initial_mode = bpy.context.active_object.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    outputFile = open(self.properties.filepath, 'w+')
    saveAnimation(self, obj, outputFile, None, None)
    outputFile.close()
    bpy.ops.object.mode_set(mode = initial_mode)
    return {'FINISHED'}