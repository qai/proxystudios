# Proxy Studios Exporters (Blender 2.90)
# Copyright (C) 2019-2020 Proxy Studios
# All rights reserved

# This add-on contains code adapted from the Snapshot Meshes add-on created by
# Vertigos and subsequently modified by AFWS. Both versions can be found at:
# https://blenderartists.org/t/addon-non-destructive-modelling-snapshot-meshes/

bl_info = {
           'name': 'Proxy Studios',
           'description': 'Mesh, Animation, and Scene Exporters',
           'author': 'Qai, Lorenz Ruhmann, Soheil Khaghani',
           'doc_url': 'https://gitlab.com/qai/proxystudios',
           'version': (1, 0, 3),
           'blender': (2, 90, 0),
           'category': 'Import-Export',
           'location': 'File > Import-Export',
           'support': 'COMMUNITY',
          }

import bpy
from bpy.props import BoolProperty, EnumProperty, IntProperty, StringProperty
from bpy.types import Operator, TOPBAR_MT_file_export
from bpy_extras.io_utils import ExportHelper, ImportHelper
from . import MeshExporter, AnimationExporter, SceneExporter, SnapshotMeshes

mesh_version_items = [
                      ('mesh_version_gladius', 'Gladius', ''),
                     ]
mesh_type_items = [
                   ('mesh_type_unit', 'Unit', ''),
                   ('mesh_type_environment', 'Environment', ''),
                  ]
animation_version_items = [
                           ('animation_version_gladius', 'Gladius', ''),
                          ]
scene_version_items = [
                       ('scene_version_gladius', 'Gladius', ''),
                      ]

class PS_OT_mesh_export(Operator, ExportHelper):
    bl_idname = 'ps.mesh_export'
    bl_label = 'Export Mesh'
    bl_description = 'Proxy Studios Mesh Exporter'
    filename_ext = '.xml'
    filter_glob : StringProperty(default='*.xml', options={'HIDDEN'})
    use_filter_folder = True
    ps_mesh_version : EnumProperty(
        name = '',
        description = 'Mesh Version',
        items = mesh_version_items,
        )
    ps_mesh_type : EnumProperty(
        name = '',
        description = 'Mesh Type',
        items = mesh_type_items,
        )
    ps_mesh_skin : BoolProperty(
        name = 'Skinned Mesh',
        description = 'Export Skinned Mesh',
        default = True,
        )
    ps_mesh_vertices : BoolProperty(
        name = 'Auto Reveal Vertices',
        description = 'Automatically Reveal Any Hidden Vertices',
        default = True,
        )
    def draw(self, context):
        layout = self.layout
        box = layout.box()
        col = box.column()
        row = col.row()
        row.label(text = 'Version')
        row.prop(self, 'ps_mesh_version')
        row = col.row()
        row.label(text = 'Type')
        row.prop(self, 'ps_mesh_type')
        row = col.row()
        row.prop(self, 'ps_mesh_skin')
        row = col.row()
        row.prop(self, 'ps_mesh_vertices')
    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
    def execute(self, context):
        return MeshExporter.save(self)

class PS_OT_animation_export(Operator, ExportHelper):
    bl_idname = 'ps.animation_export'
    bl_label = 'Export Animation'
    bl_description = 'Proxy Studios Animation Exporter'
    filename_ext = ''
    filter_glob : StringProperty(default='*.xml;*.csv', options={'HIDDEN'})
    use_filter_folder = True
    ps_animation_version : EnumProperty(
        name = '',
        description = 'Animation Version',
        items = animation_version_items,
        )
    ps_animation_frame_start : IntProperty(
        name = '',
        description = 'Animation starting frame',
        default = 0,
        min = 0,
        soft_min = 0,
        )
    ps_animation_frame_end : IntProperty(
        name = '',
        description = 'Animation ending frame',
        default = 0,
        min = 0,
        soft_min = 0,
        )
    ps_animation_batch_export : BoolProperty(
        name = 'Batch Export',
        description = 'Batch Export',
        default = False,
        )
    def draw(self, context):
        layout = self.layout
        box = layout.box()
        col = box.column()
        row = col.row()
        row.label(text = 'Version')
        row.prop(self, 'ps_animation_version')
        row = col.row()
        row.label(text = 'Starting Frame')
        row.prop(self, 'ps_animation_frame_start')
        row = col.row()
        row.label(text = 'Ending Frame')
        row.prop(self, 'ps_animation_frame_end')
        row = col.row()
        row.prop(self, 'ps_animation_batch_export')
    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
    def execute(self, context):
        return AnimationExporter.save(self)

class PS_OT_scene_export(Operator, ExportHelper):
    bl_idname = 'ps.scene_export'
    bl_label = 'Export Scene'
    bl_description = 'Proxy Studios Scene Exporter'
    filename_ext = '.xml'
    filter_glob : StringProperty(default='*.xml', options={'HIDDEN'})
    ps_scene_version : EnumProperty(
        name = '',
        description = 'Scene Version',
        items = scene_version_items,
        )
    def draw(self, context):
        layout = self.layout
        box = layout.box()
        col = box.column()
        row = col.row()
        row.label(text = 'Version')
        row.prop(self, 'ps_scene_version')
    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
    def execute(self, context):
        return SceneExporter.save(self)

class PS_OT_captureSnapShot(bpy.types.Operator):
    bl_idname = "ps.capturesnapshot"
    bl_label = "Capture Snapshot Mesh"
    bl_description = "Captures and saves the current mesh data"
    bl_options = {'REGISTER', 'UNDO', 'INTERNAL'}
    def execute(self,context):
        dyntopo = False
        current_mode = bpy.context.active_object.mode
        if current_mode == 'SCULPT':     
            sclpt_obj = bpy.context.sculpt_object 
            use_dyntopo = sclpt_obj.use_dynamic_topology_sculpting
            if use_dyntopo:
                dyntopo = True
                bpy.ops.sculpt.dynamic_topology_toggle()            
            else:
                dyntopo = False
        snapShot = SnapshotMeshes.createSnapshotMesh(bpy.context.active_object)
        ssmList = SnapshotMeshes.collectSnapShotMeshes(bpy.context.active_object)
        SnapshotMeshes.updateSnapshotMeshList(ssmList, context.object.snapShotMeshes,bpy.context.object.snapShotMesh_ID_index, snapShot)
        bpy.context.object.snapShotMesh_ID_index = len(context.object.snapShotMeshes.items())-1
        if dyntopo == True:
            bpy.ops.sculpt.dynamic_topology_toggle()
        return {'FINISHED'}
     
class PS_OT_deleteSnapShot(bpy.types.Operator):
    bl_idname = "ps.deletesnapshot"
    bl_label = "Delete Snapshot Mesh"
    bl_description = "Delete the selected mesh data"
    bl_options = {'REGISTER', 'UNDO'}
    def execute(self,context):
        dyntopo = False        
        current_mode = bpy.context.active_object.mode
        if current_mode == 'SCULPT':     
            sclpt_obj = bpy.context.sculpt_object 
            use_dyntopo = sclpt_obj.use_dynamic_topology_sculpting
            if use_dyntopo:
                dyntopo = True
                bpy.ops.sculpt.dynamic_topology_toggle()            
            else:
                dyntopo = False        
        listId = bpy.context.object.snapShotMesh_ID_index
        if listId > -1:
            try:
                ssMeshObjectName = context.object.snapShotMeshes[listId].snapShotMeshName
                SnapshotMeshes.deleteSnapshotMesh(bpy.context.active_object, ssMeshObjectName)
                ssmList = SnapshotMeshes.collectSnapShotMeshes(bpy.context.active_object)
                SnapshotMeshes.updateSnapshotMeshList(ssmList, context.object.snapShotMeshes, bpy.context.object.snapShotMesh_ID_index)
                if listId > 0: 
                    bpy.context.object.snapShotMesh_ID_index = listId - 1
                else:
                    bpy.context.object.snapShotMesh_ID_index = 0
            except:
                pass
        if dyntopo == True:
            bpy.ops.sculpt.dynamic_topology_toggle()
        return {'FINISHED'}
     
class PS_OT_deleteAllSnapShot(bpy.types.Operator):
    bl_idname = "ps.deleteallsnapshot"
    bl_label = "Delete All Snapshot Meshes"
    bl_description = "Delete all snapshot meshes"
    bl_options = {'REGISTER', 'UNDO'}
    def execute(self,context):
        dyntopo = False
        current_mode = bpy.context.active_object.mode
        if current_mode == 'SCULPT':     
            sclpt_obj = bpy.context.sculpt_object 
            use_dyntopo = sclpt_obj.use_dynamic_topology_sculpting
            if use_dyntopo:
                dyntopo = True
                bpy.ops.sculpt.dynamic_topology_toggle()            
            else:
                dyntopo = False        
        SnapshotMeshes.deleteAllSnapshotMesh(bpy.context.active_object)
        context.object.snapShotMeshes.clear()
        bpy.context.object.snapShotMesh_ID_index = -1
        if dyntopo == True:
            bpy.ops.sculpt.dynamic_topology_toggle()
        return {'FINISHED'}
     
class PS_OT_deleteUnusedSnapShotList(bpy.types.Operator):
    bl_idname = "ps.deleteunusedsnapshotlist"
    bl_label = "Delete unused Snapshot Mesh List"
    bl_description = "Deletes unused Snapshot mesh list"
    bl_options = {'REGISTER', 'UNDO'}
    def execute(self,context):
        SnapshotMeshes.deleteUnusedSnapShotMeshes()
        return {'FINISHED'}
 
class PS_OT_recalculateFromChildren(bpy.types.Operator):
    bl_idname = "ps.recalculatesnapshotfromchildren"
    bl_label = "Recalculate Snapshot Meshes From Object"
    bl_description = "Recalculate snapshot meshes. Needed in somecase (ie:duplicated objects). You will loose the snapshot names"
    bl_options = {'REGISTER', 'UNDO'}
    def execute(self,context):
        dyntopo = False
        current_mode = bpy.context.active_object.mode
        if current_mode == 'SCULPT':     
            sclpt_obj = bpy.context.sculpt_object 
            use_dyntopo = sclpt_obj.use_dynamic_topology_sculpting
            if use_dyntopo:
                dyntopo = True
                bpy.ops.sculpt.dynamic_topology_toggle()            
            else:
                dyntopo = False 
        SnapshotMeshes.recalculateFromChildren(bpy.context.view_layer.objects.active ,context.object.snapShotMeshes) 
        bpy.context.object.snapShotMesh_ID_index = -1
        if dyntopo == True:
            bpy.ops.sculpt.dynamic_topology_toggle()
        return {'FINISHED'}
                
class PS_CC_snapShotMeshCollection(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(default='')
    snapShotMeshName: bpy.props.StringProperty(default="")
    snapShotMesh_ID: bpy.props.StringProperty(default="bake",options={"HIDDEN"})
         
class PS_PT_snapShotMeshes(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Snapshot Meshes"
    bl_options = {'DEFAULT_CLOSED'}
    @classmethod
    def poll(cls, context):
        obj = context.object
        return (obj and obj.type in {'MESH'})
    def draw(self,context):
        layout = self.layout
        obj = context.object
        # Create using template_list UI         
        box = layout.box()
        col = box.row()
        row = col.row()
        row.template_list('UI_UL_list', "snapShotMesh_ID", obj, "snapShotMeshes", obj, "snapShotMesh_ID_index", rows=2)
        # Add and delete buttons 
        col = row.column(align=True)
        subrow = col.row(align=True)
        subrow.operator(PS_OT_captureSnapShot.bl_idname, icon='ADD', text="")
        subrow.operator(PS_OT_deleteSnapShot.bl_idname, icon='REMOVE', text="")
        col = col.column()
        col.separator()         
        subrow = col.row(align=True)
        subrow.alignment = 'CENTER'    
        subrow.operator(PS_OT_recalculateFromChildren.bl_idname, icon='BORDERMOVE', text="")  
        col = col.column()
        col.separator()
        subrow = col.row(align=True)
        subrow.operator(PS_OT_deleteAllSnapShot.bl_idname, icon='X', text="") 
        subrow.operator(PS_OT_deleteUnusedSnapShotList.bl_idname, icon='CANCEL', text="") 

class PS_OT_messagebox(Operator):
    bl_idname = 'ps.messagebox'
    bl_label = ''
    message : bpy.props.StringProperty(
        name = '',
        description = 'Message Box',
        default = '',
        )
    def execute(self, context):
        self.report({'INFO'}, self.message)
        print(self.message)
        return {'FINISHED'}
    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)
    def draw(self, context):
        self.layout.label(text = self.message)
        self.layout.label(text = '')

def menu_ps_mesh_export(self, context):
    self.layout.operator(PS_OT_mesh_export.bl_idname, text = 'Proxy Studios Mesh (.xml)')
def menu_ps_animation_export(self, context):
    self.layout.operator(PS_OT_animation_export.bl_idname, text = 'Proxy Studios Animation (.xml)')
def menu_ps_scene_export(self, context):
    self.layout.operator(PS_OT_scene_export.bl_idname, text = 'Proxy Studios Scene (.xml)')

classes = (
           PS_OT_mesh_export,
           PS_OT_animation_export,
           PS_OT_scene_export,
           PS_OT_messagebox,
           PS_OT_captureSnapShot,
           PS_OT_deleteSnapShot,
           PS_OT_deleteAllSnapShot,
           PS_PT_snapShotMeshes,
           PS_OT_deleteUnusedSnapShotList,
           PS_OT_recalculateFromChildren,
           PS_CC_snapShotMeshCollection, 
          )

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    TOPBAR_MT_file_export.append(menu_ps_mesh_export)
    TOPBAR_MT_file_export.append(menu_ps_animation_export)
    TOPBAR_MT_file_export.append(menu_ps_scene_export)
    bpy.types.Object.snapShotMesh_ID_index = bpy.props.IntProperty(update=SnapshotMeshes.selectSnapShotMesh)
    bpy.types.Object.snapShotMeshes = bpy.props.CollectionProperty(type=PS_CC_snapShotMeshCollection)

def unregister():
    del bpy.types.Object.snapShotMeshes
    del bpy.types.Object.snapShotMesh_ID_index
    TOPBAR_MT_file_export.remove(menu_ps_scene_export)
    TOPBAR_MT_file_export.remove(menu_ps_animation_export)
    TOPBAR_MT_file_export.remove(menu_ps_mesh_export)
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)

if __name__ == '__main__':
    register()