# Proxy Studios Gladius Mesh Exporter (Blender 2.90)
# Copyright (C) 2019-2020 Proxy Studios
# All rights reserved

import bpy
import os.path
import re
from mathutils import Matrix

# Triangulate mesh
def triangulateMesh(self, mesh):
    initial_mode = bpy.context.active_object.mode
    bpy.ops.object.mode_set(mode = 'EDIT')
    if getattr(self, 'ps_mesh_vertices'):
        bpy.ops.mesh.reveal()
    bpy.ops.mesh.select_all(action = 'SELECT')
    bpy.ops.mesh.quads_convert_to_tris(quad_method = 'BEAUTY', ngon_method = 'BEAUTY')
    mesh.update_from_editmode()
    bpy.ops.object.mode_set(mode = initial_mode)
    return {'FINISHED'}

# Get rotated matrix for X front
def getRotatedMatrix(matrix):
    rm = Matrix()
    rm[0][0], rm[0][1], rm[0][2] = matrix[0][1], -matrix[0][0], matrix[0][2]
    rm[1][0], rm[1][1], rm[1][2] = matrix[1][1], -matrix[1][0], matrix[1][2]
    rm[2][0], rm[2][1], rm[2][2] = matrix[2][1], -matrix[2][0], matrix[2][2]
    return rm

# Export vertices
def saveVertices(mesh, matrix, outputFile):
    for v in mesh.data.vertices:
        vt = matrix @ v.co
        outputFile.write('\t<vertex id=\"%d\" x=\"%.6f\" y=\"%.6f\" z=\"%.6f\"/>\n' % (v.index, vt.x, vt.y, vt.z))
    return {'FINISHED'}

# Export vertex normals
def saveVertexNormals(mesh, matrix, outputFile):
    mesh.data.calc_normals_split()
    for f in mesh.data.polygons:
        for i in f.loop_indices:
            vnt = matrix.to_3x3() @ mesh.data.loops[i].normal
            vnt.normalize()
            outputFile.write('\t<normal id=\"%d\" x=\"%.6f\" y=\"%.6f\" z=\"%.6f\"/>\n' % (i, vnt.x, vnt.y, vnt.z))
    mesh.data.free_normals_split()
    return {'FINISHED'}

# Export texture coordinates
def saveTextureCoordinates(mesh, outputFile):
    for f in mesh.data.polygons:
        for i in f.loop_indices:
            l = mesh.data.loops[i]
            for ul in mesh.data.uv_layers:
                outputFile.write('\t<textureCoordinate id=\"%d\" x=\"%.6f\" y=\"%.6f\"/>\n' % (i, ul.data[l.index].uv.x, ul.data[l.index].uv.y))
    return {'FINISHED'}

# Export faces
def saveFaces(mesh, outputFile):
    for f in mesh.data.polygons:
        outputFile.write('\t<face id=\"%d\">\n' % f.index)
        for v in f.vertices:
            outputFile.write('\t\t<vertex id=\"%d\"/>\n' % mesh.data.vertices[v].index)
        for i in f.loop_indices:
            outputFile.write('\t\t<textureCoordinate id=\"%d\"/>\n' % i)
        vcount = 0
        for v in f.vertices:
            outputFile.write('\t\t<normal id=\"%d\"/>\n' % ((f.index * 3) + vcount))
            vcount += 1
        outputFile.write('\t</face>\n')
    return {'FINISHED'}

# Export bounding geometry
def saveBoundingGeometry(mesh, matrix, outputFile):
    empties = [obj for obj in bpy.data.objects[mesh.name].children if obj.type in 'EMPTY']
    for e in empties:
        m = matrix @ e.matrix_local
        p = m.to_translation()
        rm = getRotatedMatrix(m)
        r = rm.to_quaternion()
        d = rm.to_scale() * e.empty_display_size * 2
        if e.empty_display_type in 'CUBE':
            outputFile.write('\t<orientedBoundingBox name=\"%s\" position=\"%.6f %.6f %.6f\" rotation=\"%.6f %.6f %.6f %.6f\" length=\"%.6f\" width=\"%.6f\" height=\"%.6f\"/>\n' % (e.name, p.x, p.y, p.z, r.x, r.y, r.z, r.w, d.x, d.y, d.z))
            continue
    return {'FINISHED'}

# Export bones
def saveBones(mesh, matrix, outputFile):
    reorderedIndex = -1
    mesh_group_names = [g.name for g in mesh.vertex_groups]
    arm = mesh.parent
    bones = []
    for b in arm.data.bones:
        bones.append(b.name)
    bones.sort(key = str.lower)
    for index,b in enumerate(bones):
        if b not in mesh_group_names:
            continue
        reorderedIndex += 1
        outputFile.write('\t<bone id=\"%d\" name=\"%s\">\n' % (reorderedIndex, bones[index]))
        for editBone in arm.data.bones:
            if editBone.name == bones[index]:
                m = matrix @ arm.matrix_world @ editBone.matrix_local
                p = m.to_translation()
                rm = getRotatedMatrix(m)
                r = rm.to_quaternion()
                s = rm.to_scale()
                outputFile.write('\t\t<position x=\"%.6f\" y=\"%.6f\" z=\"%.6f\"/>\n' % (p.x, p.y, p.z))
                outputFile.write('\t\t<rotation x=\"%.6f\" y=\"%.6f\" z=\"%.6f\" w=\"%.6f\"/>\n' % (r.x, r.y, r.z, r.w))
                outputFile.write('\t\t<scale x=\"%.6f\" y=\"%.6f\" z=\"%.6f\"/>\n' % (s.x, s.y, s.z))
                outputFile.write('\t</bone>\n')
                continue
    return {'FINISHED'}

# Export bone weights
def saveBoneWeights(mesh, outputFile):
    arm = mesh.parent
    bones = []
    for b in arm.data.bones:
        bones.append(b.name)
    bones.sort(key = str.lower)
    weights = []
    weightsBelowThreshold = []
    weightThreshold = 0.001
    mesh_group_names = [g.name for g in mesh.vertex_groups]
    for v in mesh.data.vertices:
        outputFile.write('\t<boneWeights id=\"%d\">\n' % (v.index))
        # Get the total weighting for all bones
        weights.clear()
        normalizedWeighting = 0
        for index,bone in enumerate(bones):
            if bone not in mesh_group_names:
                continue
            group_index = mesh.vertex_groups[bone].index
            for g in v.groups:
                if g.group == group_index:
                    weights.append(g.weight)
                    normalizedWeighting += g.weight
        # Determine which bone weights are below the threshold
        weightsBelowThreshold.clear()
        for index,w in enumerate(weights):
            if w/normalizedWeighting < weightThreshold:
                weightsBelowThreshold.append(index)
        # Remove weighting influences from bones below the threshold
        for index,w in enumerate(weights):
                for i in weightsBelowThreshold:
                    if index == i:
                        normalizedWeighting -= w
                        continue
        # Save only the bone weights above the threshold
        reorderedIndex = -1
        for index,bone in enumerate(bones):
            if bone not in mesh_group_names:
                continue
            reorderedIndex += 1
            group_index = mesh.vertex_groups[bone].index
            for g in v.groups:
                if g.group == group_index:
                    if g.weight/normalizedWeighting >= weightThreshold:
                        outputFile.write('\t\t<bone id=\"%d\" weight=\"%.6f\"/>\n' % (reorderedIndex, g.weight/normalizedWeighting))
        outputFile.write('\t</boneWeights>\n')
    return {'FINISHED'}

# Determine if exporting Unit or Environment
def saveModel(self, mesh, outputFile):
    bpy.ops.ps.capturesnapshot()
    mesh_type = getattr(self, 'ps_mesh_type')
    if mesh_type in ('mesh_type_unit'):
        result = saveUnit(self, mesh, outputFile)
    elif mesh_type in ('mesh_type_environment'):
        result = saveEnvironment(mesh, outputFile)
    numSnapshots = len(bpy.context.object.snapShotMeshes)
    bpy.context.object.snapShotMesh_ID_index = numSnapshots - 1
    bpy.ops.ps.deletesnapshot()
    return result

# Export Unit
def saveUnit(self, mesh, outputFile):
    triangulateMesh(self, mesh)
    outputFile.write('<?xml version=\"1.0\" encoding=\"utf-8\"?>\n')
    outputFile.write('<mesh>\n')
    matrix = Matrix()
    matrix.identity()
    if mesh.parent and mesh.parent.type in 'ARMATURE' and getattr(self, 'ps_mesh_skin'):
        pattern = re.compile(r'(.+)@(.+)')
        meshName = re.search(pattern, mesh.name)
        if meshName:
            arm = mesh.parent
            bones = []
            for b in arm.data.bones:
                bones.append(b.name)
            if meshName.group(2) not in bones:
                bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! Mesh does not have associated bone!')
                return {'CANCELLED'}
            attachmentBone = arm.data.bones[meshName.group(2)]
            m = arm.matrix_world @ attachmentBone.matrix_local
            mt = getRotatedMatrix(m)
            mt[0][3], mt[1][3], mt[2][3] = m[0][3], m[1][3], m[2][3]
            matrix = mt.inverted()
        saveBones(mesh, matrix, outputFile)
    meshMatrix = matrix @ mesh.matrix_world
    saveBoundingGeometry(mesh, meshMatrix, outputFile)
    saveVertices(mesh, meshMatrix, outputFile)
    saveTextureCoordinates(mesh, outputFile)
    saveVertexNormals(mesh, meshMatrix, outputFile)
    if mesh.parent and mesh.parent.type in 'ARMATURE' and getattr(self, 'ps_mesh_skin'):
        saveBoneWeights(mesh, outputFile)
    saveFaces(mesh, outputFile)
    outputFile.write('</mesh>\n')
    return {'FINISHED'}

# Export Environment
def saveEnvironment(self, mesh, outputFile):
    triangulateMesh(mesh)
    outputFile.write('<?xml version=\"1.0\" encoding=\"utf-8\"?>\n')
    outputFile.write('<mesh version=\"1.0\">\n')
    outputFile.write('\t<vertexLayout>\n')
    outputFile.write('vertexPosition 3\n')
    outputFile.write('vertexTextureCoordinate 2\n')
    outputFile.write('vertexNormal 3\n')
    outputFile.write('vertexTangent 4\n')
    outputFile.write('vertexOrigin 4\n')
    outputFile.write('\t</vertexLayout>\n')
    numFaces = len(mesh.data.polygons)
    verticesCount = numFaces * 3
    outputFile.write('\t<vertices count=\"%d\">\n' % verticesCount)
    mesh.data.calc_normals_split()
    for f in mesh.data.polygons:
        for i in f.loop_indices:
            l = mesh.data.loops[i]
            v = mesh.data.vertices[l.vertex_index].co
            vn = mesh.data.loops[i].normal
            for ul in mesh.data.uv_layers:
                outputFile.write('%.6f %.6f %.6f %.6f %.6f %.6f %.6f %.6f 0 0 0 0 0 0 0 0\n' % (v.x, v.y, v.z, ul.data[l.index].uv.x, 1 - ul.data[l.index].uv.y, vn.x, vn.y, vn.z))
    mesh.data.free_normals_split()
    outputFile.write('\t</vertices>\n')
    outputFile.write('</mesh>\n')
    return {'FINISHED'}

# Export meshes
def save(self):
    meshObjects = [obj for obj in bpy.context.selected_objects if obj.type in 'MESH']
    if len(meshObjects) == 0:
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! No mesh selected!')
        return {'FINISHED'}
    if len(meshObjects) != len(bpy.context.selected_objects):
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! Must only select mesh objects!')
        return {'FINISHED'}
    exportedObjectsNames = [mesh.name for mesh in meshObjects]
    initial_mode = bpy.context.active_object.mode
    initial_active_object_name = bpy.context.view_layer.objects.active.name
    bpy.ops.object.mode_set(mode = 'OBJECT')
    result = {'FINISHED'}
    if os.path.isdir(self.properties.filepath):
        bpy.ops.object.select_all(action = 'DESELECT')
        for mesh in meshObjects:
            outputFile = open(self.properties.filepath + mesh.name + '.xml', 'w+')
            bpy.data.objects[mesh.name].select_set(True)
            bpy.context.view_layer.objects.active = bpy.data.objects[mesh.name]
            result = saveModel(self, mesh, outputFile)
            bpy.data.objects[mesh.name].select_set(False)
            outputFile.close()
        for mesh in meshObjects:
            bpy.data.objects[mesh.name].select_set(True)
            bpy.context.view_layer.objects.active = bpy.data.objects[initial_active_object_name]
    else:
        # Join multiple meshes into a single mesh
        meshObjectsCount = len(meshObjects)
        meshObjectsNames = [mesh.name for mesh in meshObjects]
        bpy.ops.object.select_all(action = 'DESELECT')
        for mesh in meshObjects:
            mesh.select_set(True)
        bpy.ops.object.join()
        bpy.ops.object.origin_set(type = 'ORIGIN_GEOMETRY')
        mesh = bpy.context.selected_objects[0]
        outputFile = open(self.properties.filepath, 'w+')
        result = saveModel(self, mesh, outputFile)
        outputFile.close()
        # Revert the joining of multiple meshes
        bpy.ops.object.select_all(action = 'DESELECT')
        if meshObjectsCount > 1:
            bpy.ops.ed.undo()
        for mesh in meshObjectsNames:
            bpy.data.objects[mesh].select_set(True)
        for mesh in meshObjectsNames:
            if mesh == initial_active_object_name:
                bpy.context.view_layer.objects.active = bpy.data.objects[mesh]
                break
    bpy.ops.object.mode_set(mode = initial_mode)
    if result == {'FINISHED'}:
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'SUCCESS! Exported ' + str(len(exportedObjectsNames)) + ' object(s): ' + ', '.join(exportedObjectsNames) + '.')
    return {'FINISHED'}