# Proxy Studios Exporters (Blender 2.90)
# Copyright (C) 2019-2020 Proxy Studios
# All rights reserved

# This add-on contains code adapted from the Snapshot Meshes add-on created by
# Vertigos and subsequently modified by AFWS. Both versions can be found at:
# https://blenderartists.org/t/addon-non-destructive-modelling-snapshot-meshes/

import bpy

def findSnapShotData(p_obj,p_string):
    isSnapShot = -1
    upp_name = p_obj.name.upper()
    upp_string = p_string.upper()
    isSnapShot= upp_name.find(upp_string)
    return isSnapShot
  
def findSnapShot(p_obj, p_snapShotName):
    undoSnapShot = None
    for o in p_obj.children:
        finder = findSnapShotData(o,p_snapShotName)
        if finder >= 0:
            undoSnapShot = o
            break
    return undoSnapShot
 
def findSnapshotMeshListChild(p_obj):
    ssMeshList = None
    for o in p_obj.children:
        finder = findSnapShotData(o,"snapshotMeshList")
        if finder >= 0:
            ssMeshList = o
            break
    return ssMeshList

def createSnapshotMesh(p_obj):
    current_mode = p_obj.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    obj = p_obj
    bpy.ops.object.select_all(action='DESELECT')
    obj.select_set(state=True)
    bpy.context.view_layer.objects.active = obj
    snapShotName = obj.name + "_snapshotMesh.000"
    snapShotMesh = bpy.data.meshes.new("snapshotMeshData")
    snapShot = bpy.data.objects.new(snapShotName, snapShotMesh)
    snapShot.data = obj.data.copy()
    bpy.context.collection.objects.link(snapShot)
    #If snapshot list is empty
    ssMeshList = findSnapshotMeshListChild(obj)
    if ssMeshList == None:
        bpy.ops.object.empty_add(type='ARROWS', location =(0,0,0))
        emptySnapShotList = bpy.context.view_layer.objects.active
        emptySnapShotList.name = "snapshotMeshList.000"
        emptySnapShotList.location = obj.location
        emptySnapShotList.parent = obj
        snapShot.parent = emptySnapShotList
        ssMeshList = emptySnapShotList         
    else:
        snapShot.parent = ssMeshList
    bpy.ops.object.parent_set(type='OBJECT', keep_transform=False)
    bpy.ops.object.select_all(action='DESELECT')
    ssMeshList.select_set(state=False)
    ssMeshList.hide_viewport = True
    ssMeshList.hide_select = True
    ssMeshList.hide_render = True
    snapShot.select_set(state=False)
    snapShot.hide_viewport = True
    snapShot.hide_select = True
    snapShot.hide_render = True
    bpy.ops.object.select_all(action='DESELECT')
    obj.select_set(state=True)
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.mode_set(mode = current_mode)
    return snapShot
 
def createUndoSnapshotMesh(p_obj):
    current_mode = p_obj.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    obj = p_obj
    bpy.ops.object.select_all(action='DESELECT')
    obj.select_set(state=True)
    bpy.context.view_layer.objects.active = obj
    #If snapshot list is empty
    ssMeshList = findSnapshotMeshListChild(obj)
    if ssMeshList == None:
        bpy.ops.object.empty_add(type='ARROWS', location =(0,0,0))
        emptySnapShotList = bpy.context.view_layer.objects.active
        emptySnapShotList.name = "snapshotMeshList.000"
        emptySnapShotList.location = obj.location  
        emptySnapShotList.parent = obj
        ssMeshList = emptySnapShotList
    snapShot = findSnapShot(ssMeshList , "_undo_snapshotMesh")
    if snapShot == None:
        snapShotName = obj.name + "_undo_snapshotMesh.000"
        snapShotMesh = bpy.data.meshes.new("unsdoSnapshotMeshData")
        snapShot = bpy.data.objects.new(snapShotName, snapShotMesh)
        snapShot.data = obj.data.copy()
        bpy.context.collection.objects.link(snapShot)
        snapShot.parent = ssMeshList
    else:
        snapShot.data = obj.data.copy()
    bpy.ops.object.parent_set(type='OBJECT', keep_transform=False)
    bpy.ops.object.select_all(action='DESELECT')
    ssMeshList.select_set(state=False)
    ssMeshList.hide_viewport = True
    ssMeshList.hide_select = True
    ssMeshList.hide_render = True
    snapShot.select_set(state=False)
    snapShot.hide_viewport = True
    snapShot.hide_select = True
    snapShot.hide_render = True
    bpy.ops.object.select_all(action='DESELECT')
    obj.select_set(state=True)
    bpy.context.scene.objects.active = obj
    bpy.ops.object.mode_set(mode = current_mode)
    return snapShot
     
def deleteSnapshotMesh(p_obj, p_snapshotMeshName):
    current_mode = p_obj.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    snapShot = bpy.data.objects[p_snapshotMeshName]
    snapShot.hide_viewport = False
    snapShot.hide_select = False
    bpy.ops.object.select_all(action='DESELECT')
    snapShot.select_set(state=True)
    bpy.context.view_layer.objects.active = snapShot
    bpy.ops.object.delete()
    bpy.ops.object.select_all(action='DESELECT')
    p_obj.select_set(state=True)
    bpy.context.view_layer.objects.active = p_obj
    bpy.ops.object.mode_set(mode = current_mode)
    return p_obj
 
def deleteAllSnapshotMesh(p_obj):
    current_mode = p_obj.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    ssMeshList = findSnapshotMeshListChild(p_obj)
    if ssMeshList != None:    
        for c in ssMeshList.children:
            finder = findSnapShotData(c,"_snapshotMesh")
            if finder >= 0:
                c.hide_viewport = False
                c.hide_select = False
                bpy.ops.object.select_all(action='DESELECT')
                c.select_set(state=True)
                bpy.context.view_layer.objects.active = c
                bpy.ops.object.delete()
        ssMeshList.hide_viewport = False
        ssMeshList.hide_select = False   
        bpy.ops.object.select_all(action='DESELECT')
        ssMeshList.select_set(state=True)
        bpy.context.view_layer.objects.active = ssMeshList
        bpy.ops.object.delete()
    bpy.ops.object.select_all(action='DESELECT')
    p_obj.select_set(state=True)
    bpy.context.view_layer.objects.active = p_obj
    bpy.ops.object.mode_set(mode = current_mode)
    return {'FINISHED'}
 
def useSnapShot(p_obj, p_snapShot):
    current_mode = p_obj.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    bpy.ops.object.select_linked(type='OBDATA')
    p_obj.data = p_snapShot.data.copy()
    p_obj.select_set(state=False)
    for o in bpy.context.selected_objects:
        if o.type == 'MESH':
            o.data = p_obj.data
    bpy.ops.object.select_all(action='DESELECT')
    p_obj.select_set(state=True)
    bpy.context.view_layer.objects.active = p_obj
    bpy.ops.object.mode_set(mode = current_mode)
     
def collectSnapShotMeshes(p_obj):
    ssMeshes = []
    ssMeshList = findSnapshotMeshListChild(p_obj)
    if ssMeshList != None:    
        for c in ssMeshList.children:
            finder = findSnapShotData(c,"_snapshotMesh")
            if finder >= 0:
                ssMeshes.append(c)
    return ssMeshes
           
def collectListNames(p_UIList, p_IdList):
    listSnapShots = []
    cont = 0
    for o in p_UIList:
        if cont != p_IdList:
            listSnapShots.append(o)
        cont += 1
    return listSnapShots
   
def deleteUnusedSnapShotMeshes():
    current_mode = bpy.context.active_object.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    baseObj = bpy.context.view_layer.objects.active
    bpy.ops.object.select_all(action='DESELECT')
    for o in bpy.data.objects:
        if o.parent == None: 
            finder = findSnapShotData(o,"snapshotMesh") 
            if finder >= 0:
                for c in o.children:
                    c.hide_viewport = False
                    c.hide_select = False
                    bpy.ops.object.select_all(action='DESELECT')
                    c.select_set(state=True)
                    bpy.context.view_layer.objects.active = c
                    bpy.ops.object.delete()
                o.hide_viewport = False
                o.hide_select = False
                bpy.ops.object.select_all(action='DESELECT')
                o.select_set(state=True)
                bpy.context.view_layer.objects.active = o
                bpy.ops.object.delete()
    bpy.ops.object.select_all(action='DESELECT')
    baseObj.select_set(state=True)
    bpy.context.view_layer.objects.active = baseObj
    bpy.ops.object.mode_set(mode = current_mode)
      
def updateSnapshotMeshList(p_list, p_UIList, p_IdList, p_snapShot=None, p_undo=False):
    cont = len(p_list)
    snapShots = collectListNames(p_UIList, p_IdList)
    longSnapShots =  len(p_UIList)
    longSnapShotsMesh = len(p_list)
    if longSnapShotsMesh > longSnapShots:
        #if is added a new one add to the UI List
        sMesh = p_UIList.add()
        sMesh.name = "snapshot_" + str(longSnapShotsMesh)
        sMesh.snapShotMeshName = p_snapShot.name
    elif longSnapShotsMesh < longSnapShots:
        #if delete the snapshot from de UI List
        p_UIList.remove(p_IdList)
    return {'FINISHED'}
 
def recalculateFromChildren(p_obj,p_UIList):
    current_mode = p_obj.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    ssMeshList = findSnapshotMeshListChild(p_obj)
    cont = 1
    p_UIList.clear()
    if ssMeshList != None:
        for c in ssMeshList.children:
            sMesh = p_UIList.add()
            sMesh.name = "snapshot_" + str(cont)
            sMesh.snapShotMeshName = c.name
            cont += 1
    bpy.ops.object.mode_set(mode = current_mode)
    return {'FINISHED'}                
 
def selectSnapShotMesh(self, context):    
    current_mode = bpy.context.active_object.mode
    dyntopo = False
    if current_mode == 'SCULPT':             
        sclpt_obj = bpy.context.sculpt_object 
        use_dyntopo = sclpt_obj.use_dynamic_topology_sculpting
        if use_dyntopo:
            dyntopo = True
            bpy.ops.sculpt.dynamic_topology_toggle()                 
    if self.snapShotMesh_ID_index > -1:
        try:
            ssMeshName = context.object.snapShotMeshes[self.snapShotMesh_ID_index].snapShotMeshName
            snapShotMesh = bpy.data.objects[ssMeshName]
            useSnapShot(bpy.context.active_object, snapShotMesh)
        except:
            pass
    if dyntopo == True:
        bpy.ops.sculpt.dynamic_topology_toggle()