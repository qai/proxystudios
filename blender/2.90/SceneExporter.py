# Proxy Studios Gladius Scene Exporter (Blender 2.90)
# Copyright (C) 2019-2020 Proxy Studios
# All rights reserved

import bpy

# Export meshes
def saveMeshes(meshObjects, outputFile):
    for mesh in meshObjects:
        # Allow multiple instances of the same object to be exported using a single name
        meshNamePartition = mesh.name.rpartition('.')
        if not meshNamePartition[0] and not meshNamePartition[1]:
            meshName = meshNamePartition[2]
        else:
            meshName = meshNamePartition[0]
        p = mesh.matrix_world.to_translation()
        r = mesh.matrix_world.to_quaternion()
        s = mesh.matrix_world.to_scale()
        outputFile.write('\t<model mesh=\"%s\" position=\"%f %f %f\" rotation=\"%f %f %f %f\" scale=\"%f %f %f\"/>\n' % (meshName, p.x, p.y, p.z, r.x, r.y, r.z, r.w, s.x, s.y, s.z))
    return {'FINISHED'}

# Export scene
def save(self):
    meshObjects = [obj for obj in bpy.context.selected_objects if obj.type in 'MESH']
    if len(meshObjects) == 0:
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! No mesh selected!')
        return {'FINISHED'}
    if len(meshObjects) != len(bpy.context.selected_objects):
        bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'ERROR! Must only select mesh objects!')
        return {'FINISHED'}
    exportedObjectsNames = [mesh.name for mesh in meshObjects]
    initial_active_object = bpy.context.view_layer.objects.active
    initial_mode = bpy.context.active_object.mode
    bpy.ops.object.mode_set(mode = 'OBJECT')
    outputFile = open(self.properties.filepath, 'w+')
    outputFile.write('<?xml version=\"1.0\" encoding=\"utf-8\"?>\n')
    outputFile.write('<scene version=\"1.0\">\n')
    saveMeshes(meshObjects, outputFile)
    outputFile.write('</scene>\n')
    outputFile.close()
    bpy.ops.object.select_all(action = 'DESELECT')
    for mesh in meshObjects:
        bpy.data.objects[mesh.name].select_set(True)
    bpy.context.view_layer.objects.active = initial_active_object
    bpy.ops.object.mode_set(mode = initial_mode)
    bpy.ops.ps.messagebox('INVOKE_DEFAULT', message = 'SUCCESS! Exported ' + str(len(meshObjects)) + ' object(s): ' + ', '.join(exportedObjectsNames) + '.')
    return {'FINISHED'}